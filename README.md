# botrandwalk

Requirements:
- numpy
- python-twitter

Change the API-key values in twitter_config.py

You can use get_handles.py to grab some 'random' handles. Then run graph_traversal.py
to check the handles. graph_analyser.py contains a class to analyse the graph
you generated.