"""
Author: Eran Lambooij
Date: 09-02-2019


"""
from __future__ import print_function # make print statements in python2 ok
import json
import logging

from cluster import Cluster, Strain
from datasource import FavoritesDataSource
from twitter_config import api_keys


def experiments(handle_fname, output_fname, verified_users_fname=None, repetitions=10, cluster=None):
    print("start_experiment")
    handles = []
    # Retrieve the handles we want to use in the experiment
    with open(handle_fname, "r") as f:
        for line in f:
            handles.append(line.strip())
    # remove duplicates from the handles list
    handles = list(set(handles))
    
    
    verified_users = []
    if verified_users_fname is not None:
        with open(verified_users_fname, "r") as f:
            for line in f:
                verified_users.append(line.strip())
    # remove duplicates
    verified_users = list(set(verified_users))

    # we run this experiment on the favourites
    data_source = FavoritesDataSource(keys=api_keys, threaded=True)
    
    # If a cluster is given use that one as a base otherwise start a new one
    if cluster is None:
        fc = MultiProcessCluster(handles, data_source=data_source, verified_users=verified_users)
    else:
        fc = cluster
        fc.data_source = data_source
        fc.verified_users = verified_users

    try:
        fc.get_strains(handles=handles, strains_per_handle=repetitions, reentry=True)
    except KeyboardInterrupt:
        # We don't want to crash the program on a keyboard interrupt and lose all
        # harvested data, so we gracefully exit
        print("Stopping the experiment and writing everything the results to: %s"%output_fname)

    fc.save(output_fname)


if __name__ == "__main__":
    import sys

    logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
    logger = logging.getLogger('graphTraversal')
    # disabling the requests and urllib loggers
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("requests_oauthlib").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("oauthlib").setLevel(logging.WARNING)

    if len(sys.argv) not in [4, 5, 6]:
        print("Usage: %s <twitter-handle-file> <output-file> <repetitions> [<cluster-file> <verified-users>]"%sys.argv[0])
        exit()
    if len(sys.argv) == 5:
        cluster = MultiProcessCluster.load(sys.argv[4])
    else:
        cluster = None
    if len(sys.argv) == 6:
        verified_users = sys.argv[5]
    else:
        verified_users = None

    handle_fname = sys.argv[1]
    output_fname = sys.argv[2]
    repetitions = int(sys.argv[3])

    experiments(handle_fname, output_fname, repetitions=repetitions, cluster=cluster, verified_users_fname=verified_users)
