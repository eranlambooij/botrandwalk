import twitter
import random
import logging
import time
import threading
import requests
from .cache import Cache, ThreadSafeCache

logger = logging.getLogger(__name__)

class DataSource(object):
    """
    Base class of a data source
    """
    rate = 0

    def get_friends(self, user, callback=None):
        raise NotImplementedError()
    
    def get_user(self, handle, callback=None):
        raise NotImplementedError()

    def get_api_tokens(self, keys):
        raise NotImplementedError()

    def wait(self, wait_time_seconds=None):
        if wait_time_seconds is None:
            if self.rate == 0:
                wait_time_seconds = 0
            else:
                wait_time_seconds = 1/float(self.rate)
        
        time.sleep(wait_time_seconds)


class FavoritesDataSource(DataSource):
    """
    Twitter data source implementing DataSource 
    """
    rate = 180.0/(15*60) # max number of requests per second
    def __init__(self, keys=[], friends_cache=None, user_cache=None, threaded=False):
        self.api_tokens = self.get_api_tokens(keys) 
        self.rate = self.rate * float(len(self.api_tokens)) # we can increase the rate by the amount of keys we have
        self.current = 0
        self.friends_cache = friends_cache
        self.user_cache = user_cache

        if threaded:
            cache_cls = ThreadSafeCache
        else:
            cache_cls = Cache

        if friends_cache is None:
            self.friends_cache = cache_cls()
        if user_cache is None:
            self.user_cache = cache_cls()

        logger.info("Rate: {} per second".format(self.rate))
        print("RATE: ", self.rate)
    
    def get_api_tokens(self, keys):
        api = []
        for key in keys:
            api.append([threading.Lock(), twitter.Api(
                consumer_key = key[0],
                consumer_secret = key[1],
                access_token_key = key[2],
                access_token_secret = key[3],
                sleep_on_rate_limit=True,
                timeout=60)
                ])
        # api = self.get_working_api_tokens(api)
        return api
    
    def get_working_api_tokens(self, api_tokens):
        working_api_tokens = []
        logger.info("Checking api tokens")
        for lock, token in api_tokens:
            try:
                # do something with the token to check if it works
                with lock:
                    token.InitializeRateLimit()

                working_api_tokens.append(token) 
            except twitter.error.TwitterError as e:
                pass
        logger.info("Working API tokens: %d (out of %d)", len(working_api_tokens),
                len(api_tokens))
        return working_api_tokens

    def next_api_token(self):
        index = random.choice(range(len(self.api_tokens)))
        while not self.api_tokens[index][0].acquire(blocking=False):
            index = random.choice(range(len(self.api_tokens)))

        api = self.api_tokens[index]
        return api, index

    def get_user(self, handle):
        cached, user = self.user_cache.get_item(handle)
        if not cached:
            logger.debug("API GetUser (%s)", handle)
            (lock, api), index = self.next_api_token()
            try:
                user = api.GetUser(screen_name=handle)
            except twitter.error.TwitterError as e:
                if e.args[0][0]["code"] in [89, 326]:
                    removed_key = self.api_tokens.pop(index)
                    logger.warning("API key error, removed %s (%d)", removed_key, 
                            len(self.api_tokens))
                raise e
            finally:
                lock.release()

            self.user_cache.add_key(handle, user)
            self.wait() 
    
        return user

    def get_friends(self, user_id, max_nrof_friends=100):
        cached, friends = self.friends_cache.get_item(user_id)
        if not cached:
            logger.debug("API GetFavourites (%s)", user_id)
            lock = None
            statuses = None
            tries = 10
            retry_wait_time = 5
            while statuses is None:
                if tries == 0:
                    logger.error("Tried 10 times, couldn't get it to work, skipping... user_id=%s", user_id)
                else:
                    tries -= 1

                # retrieve the data from the API
                try:
                    (lock, api), index = self.next_api_token()
                    statuses = api.GetFavorites(
                        user_id=user_id, count=max_nrof_friends)
                except twitter.error.TwitterError as e:
                    if isinstance(e.args[0][0], dict):
                        error_code = e.args[0][0]["code"]
                    else:
                        logger.error("No Error code in TwitterError: %s, skipping this user (%s)", e, user_id)
                        return []

                    if  error_code in [89, 326]:
                        removed_key = self.api_tokens.pop(index)
                        logger.warning("API key error, removed %s (%d)", removed_key, 
                                len(self.api_tokens))
                    elif error_code == 88:
                        logger.warning("Rate Limit exceeded on: %s. Time out... (%s)", api, 5*60)
                        self.wait(60*5)
                        logger.info("Api key %s, back online, skipping this node", api)
                    else:
                        logger.error("Unexpected error, waiting a minute and trying again: %s", e)
                        self.wait(60)

                except requests.exceptions.ReadTimeout as e:
                    logger.error("Connection (read) timeout, connection down, retrying in %d seconds (%s)",
                            retry_wait_time, e)
                    self.wait(retry_wait_time)
                    retry_wait_time = min(3600, retry_wait_time*2)
                    tries = 10 # since the connectio is down, just keep waiting
                    statuses = None
                except requests.exceptions.ConnectionError as e:
                    logger.error("Connection failed retrying in %d seconds: %s",retry_wait_time,  e)
                    self.wait(retry_wait_time)
                    retry_wait_time = min(3600, retry_wait_time*2)
                    tries = 10 # since the connectio is down, just keep waiting
                    statuses = None

                finally:
                    if lock is not None:
                        lock.release()


            friends = map(lambda x: x.user, statuses)
            friends = list(filter(lambda x: x.id != user_id, friends))

            self.friends_cache.add_key(user_id, friends)

            self.wait() 

        return friends 
    
    def get_search(self, query, count=100):
        (lock, api), index = self.next_api_token()
        results = api.GetSearch(term=query, count=count)
        lock.release()

        return results

    def to_json(self):
        pass
    
    def from_json(self):
        pass

