from cluster import Cluster, MultiProcessCluster
from datasource import FavoritesDataSource
from twitter_config import *
import logging

if __name__ == "__main__":
    import sys
    
    logging.config.fileConfig('logging.conf', disable_existing_loggers=False)
    logger = logging.getLogger('graphTraversal')
    # disabling the requests and urllib loggers
    logging.getLogger("requests").setLevel(logging.WARNING)
    logging.getLogger("requests_oauthlib").setLevel(logging.WARNING)
    logging.getLogger("urllib3").setLevel(logging.WARNING)
    logging.getLogger("oauthlib").setLevel(logging.WARNING)

    if len(sys.argv) != 3:
        print("Usage: %s <handle> <repetitions>")
        exit()

    nrof_strains = int(sys.argv[2])
    data_source = FavoritesDataSource(api_keys, threaded=False)
    cl = MultiProcessCluster([sys.argv[1]], data_source)
    cl.nrof_processes = nrof_strains
    cl.get_strains(strains_per_handle=nrof_strains, intermediate_save=False)

    print("DONE - printing cluster")

    print(cl)
