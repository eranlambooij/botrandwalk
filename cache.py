import sys
import logging
import threading

logger = logging.getLogger(__name__)

class Cache(object):
    """
    This class implements a simple cache that can be used to save on API calls.
    """
    max_size = 500 # the maximum number of cache items +/- 1GB

    def __init__(self):
        self.cache = {}
        self.current_index = 0
     
    def add_key(self, key, item):
        if key not in self.cache:
            self.cache[key] = (self.current_index, item)
            self.current_index += 1
            
        if len(self.cache) > self.max_size:
            self.cull_cache(self.max_size/3)

    def get_item(self, key):
        if key in self.cache:
            self.cache[key] = (self.current_index, self.cache[key][1])
            return True, self.cache[key][1]
        else:
            return False, None

    def cull_cache(self, max_items=1000):
        new_cache = {}
        for key, value in self.cache.items():
            if self.cache[key][0] >= self.current_index - max_items:
                new_cache[key] = value
        self.cache = new_cache
        logger.info("Cache Culled to %d items", len(self.cache)) 

    def to_json(self):
        pass
    
    def from_json(self):
        pass

class ThreadSafeCache(Cache):
    def __init__(self):
        self.lock = threading.Lock()
        super().__init__()

    def add_key(self, key, item):
        with self.lock:
            super().add_key(key, item)

    def get_item(self, key):
        with self.lock:
            item = super().get_item(key)
        return item

    def cull_cache(self, max_items=1000):
        with self.lock:
            super().cull_cache(max_items)

class NoItemsCache(ThreadSafeCache):
    max_size = 2**20
    max_bytes = 4 * 2**30 # 4 gigabyte

    def __init__(self):
        super().__init__()
        self.cache = set()

    def add_key(self, key, item=None):
        with self.lock:
            self.cache.add(key)
        # Cull the cache if it is too large
        if len(self.cache) > self.max_size:
            self.cull_cache(self.max_size/3)
        elif sys.getsizeof(self.cache) > self.max_bytes:
            current_size = len(self.cache)
            self.cull_cache(current_size/3)

    
    def get_item(self, key):
        return key in self.cache, None

