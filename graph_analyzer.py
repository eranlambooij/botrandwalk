from __future__ import print_function # make print statements in python2 ok
from graph_traversal import Cluster
import numpy as np

class Analyzer:
    def __init__(self, cluster):
        self.cluster = cluster

    def get_strain_lengths(self, method="avg"):
        lengths = {}
        if method == "avg":
            for handle, strains in self.cluster.strains.items():
                lengths[handle] = sum(map(len, strains))/len(strains)
        elif method == "median":
            for handle, strains in self.cluster.strains.items():
                lengths[handle] = np.median(map(len, strains))
        elif method == "all":
            for handle, strains in self.cluster.strains.items():
                lengths[handle] = map(len, strains)
        else:
            print("Method not allowed")
        return lengths
    
    def get_self_intersections(self):
        for handle, strains in self.cluster.strains.items():
            first = set(map(lambda x: x.screen_name, strains[0].strain)) - set(handle)
            intersections = []
            print(handle)
            for strain in strains[1:]:
                inter = first & set(map(lambda x: x.screen_name, strain.strain))
                if len(inter) > 0:
                    print("\t", list(inter))
     
    def get_some_measure(self, factor=1.0):
        sorted_strains = self.cluster.strains.items() 
        sorted_strains.sort(key=lambda x: x[0].upper())
        for handle, strains in sorted_strains:
            s = {}
            for strain in strains:
                for handle_ in [x.screen_name for x in strain.strain]:
                    s[handle_] = s.get(handle_, 0) + 1
            
            a = []
            for handle_, count in s.items():
                if count >= len(strains)*factor and handle != handle_:
                    a.append((handle_, count))
            if len(a) > 0:
                print(handle, len(strains), a)
    
    def get_focal_points(self):
        """
        This method tries to find focal points by counting incoming paths for
        each node
        """
        nodes = {}
        nodes_uniq = {}
        for handle, strains in self.cluster.strains.items():
            for strain in strains:
                for node in strain.strain[1:]:
                    if node.screen_name in nodes:
                        nodes[node.screen_name] = (nodes[node.screen_name][0] + 1, node)
                        # nodes_uniq[node.screen_name].add(node)
                    else:
                        nodes[node.screen_name] = (1, node)
                        # nodes_uniq[node.screen_name] = set(node)

        nodes = nodes.items()
        nodes.sort(key = lambda x: x[1][0])
        nodes = filter(lambda x: x[1][0] > 5, nodes)

        for n in nodes:
            print("%16s\t%4d\t%s"%(n[0], n[1][0], n[1][1].verified == True))
    
    def get_strains_around(self, screen_name):
        strains_around = []
        for handle, strains in self.cluster.strains.items():
            for strain in strains:
                if screen_name in map(lambda x: x.screen_name, strain.strain):
                    strains_around.append(strain)
        
        for s in strains_around:
            print(s)

    def get_info(self):
        print("Cluster size: %d"%len(self.cluster.strains))
        print("Nrof strains: %d"%sum(map(len, self.cluster.strains)))


if __name__ == "__main__":
    import sys

    if len(sys.argv) != 2:
        print("Usage: %s <cluster_fname>", sys.argv[0])
        exit()
    
    cl = Cluster.load(sys.argv[1])
    an = Analyzer(cl)

    # print(cl)
    # tot = 0
    # for handle, length in  an.get_strain_lengths("median").items():
    #     if length < 4:
    #         print "%15s %0.1f"%(handle, length)
    #         tot += 1
    # print tot

    # an.get_self_intersections()
    # print("\nFactor 0.5")
    # an.get_some_measure(factor=.5)
    # print("\nFactor 1.0")
    # an.get_some_measure(factor=1.0)
    # print("\nFactor 1.5")
    # an.get_some_measure(factor=1.5)
    
    an.get_focal_points()
    # an.get_info()

    # an.get_strains_around("maryjanesosexy")
