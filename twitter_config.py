# Substitute the values of api-keys for your api-keys. You can add multiple
# keys to increase the rate of querying the API.
api_keys = [
    (
        "CONSUMER-KEY",
        "CONSUMER-SECRET",
        "ACCESSTOKEN-KEY",
        "ACCESSTOKEN-SECRET"
    ),
]
