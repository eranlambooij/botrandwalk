"""
Author: Eran Lambooij
Date: 8-03-2019
"""
import random
import numpy as np
import time
import collections
import logging
import logging.config
import twitter
import os
import secrets
import multiprocessing
from multiprocessing.pool import ThreadPool
from .twitter_config import api_keys
import json

from .datasource import DataSource

logger = logging.getLogger(__name__)

class Strain(object):
    """
    A strain is a walk through the social graph.
    
    You can initialize a strain with a source. The walk is computed by calling
    generate on the strain.

    The function next 
    """
    def __init__(self, data_source=None, handle=None, verified_users=None):
        if data_source is None:
            data_source = DataSource()
        if verified_users is None:
            verified_users = []
        self.data_source = data_source
        self.verified_users = verified_users

        if handle is None:
            self.strain = []
        else:
            head = self.data_source.get_user(handle)
            self.strain = [head]
    
    def next(self):
        head = self.strain[-1]
        friends = self.data_source.get_friends(head.id)

        if len(friends) < 1:
            logger.debug("No Friends found.")
            return "NoFriends"

        # pick a friend at random
        friends, prob = list(zip(*collections.Counter(friends).items()))
        prob = [x*x for x in prob]
        total = sum(prob)
        prob = [float(x)/float(total) for x in prob]

        try:
            n = np.random.RandomState().choice(friends, 1, p=prob)[0]
        except IndexError:
            return False 

        self.strain.append(n)
    
        logger.debug("%s", n.screen_name)

        if self.ishuman(n):
            return False
        else:
            return True

    def stop_condition(self):
        return len(self.strain) > 50

    def ishuman(self, user):
        return user.verified or user.screen_name in self.verified_users
     
    def generate(self):
        flag = True
        nrof_retries = 0
        while flag:
            try:
                flag = self.next()
                if flag == "NoFriends":
                    flag = True
                    nrof_retries += 1
                    self.strain = [self.strain[0]]
                    if nrof_retries > 5:
                        flag = False

            except twitter.error.TwitterError as e:
                logger.warning("TWITTER ERROR: %s", e)
                if e.message == "Not authorized.":
                    flag = False
                    continue
                time.sleep(60)
            # test for abnormal conditions (such as very long chains) and break
            if self.stop_condition():
                break
        return self.strain
    
    def __repr__(self):
        s = u""
        for n in self.strain:
            s += u"%s -> "%n.screen_name

        s = s.strip("-> ")
        return s#.encode("utf-8", errors="replace")

    def __len__(self):
        return len(self.strain)

    def self_intersection(self):
        return collections.Counter(map(lambda x: x.id, self.strain))
    
    def intersection(self, other):
        s = set(map(lambda x: x.name, self.strain))
        s2 = set(map(lambda x: x.name, other))
            
        return s.intersection(s2)
    
    def to_json(self):
        json_strain = []
        for s in self.strain:
            json_strain.append(s.AsJsonString())
    
        return json.dumps(json_strain)

    @classmethod
    def from_json(cls, s):
        json_strain = json.loads(s)
        
        c = cls()
        for js in json_strain:
            c.strain.append(twitter.User.NewFromJsonDict(json.loads(js)))
    
        return c

class Cluster(object):
    def __init__(self, handles, data_source=None, verified_users=None):
        if data_source is None:
            data_source = DataSource()
        if verified_users is None:
            verified_users = []

        self.handles = handles
        self.data_source = data_source
        self.strains = {}
        self.verified_users = verified_users
   
    def compute_strains(self, handle, strains_to_compute):
        for i in range(strains_to_compute):
            try:
                strain = Strain(self.data_source, handle, 
                        verified_users=self.verified_users)
                strain.generate()
                if handle not in self.strains:
                    self.strains[handle] = [strain]
                else:
                    self.strains[handle].append(strain)

            except twitter.error.TwitterError as e:
                logger.warning("Twitter error: %s", e)
                if e.message == "Not authorized.":
                    continue

    def get_strains(self, handles=None, strains_per_handle=1, intermediate_save=True, 
                    reentry=False):
        if handles is None:
            handles = self.handles

        for handle in handles:
            if handle not in self.handles:
                self.handles.append(handle)
            if reentry and handle in self.strains:
                # if reentry is set only compute the strains to top up 
                # to strains_per_handle
                strains_to_compute = max(0, strains_per_handle - 
                        len(self.strains[handle]))
            else:
                strains_to_compute = strains_per_handle

            logger.info("User %s (%d/%d)", handle, strains_to_compute, 
                    strains_per_handle)

            self.compute_strains(handle, strains_to_compute)

            if handle in self.strains:
                logger.info("Strains: %s", [len(x) for x in self.strains[handle]])

            if intermediate_save and strains_to_compute > 0:
                self.save("clusters.tmp")

    def to_json(self):
        strains = {} 
        for key, strain in self.strains.items():
            handle_strains = []
            for s in strain:
                handle_strains.append(s.to_json())
            strains[key] = handle_strains
    
        return json.dumps(strains)
    
    @classmethod
    def from_json(cls, json_str):
        strains = json.loads(json_str)
        c = cls(list(strains.keys()))
        for key, strain in strains.items():
            handle_strains = []
            for s in strain:
                handle_strains.append(Strain.from_json(s))
            
            c.strains[key] = handle_strains
    
        return c
    
    def __repr__(self):
        s = u""
        for handle, st in self.strains.items():
            s += u"%s: \n"%handle
            s += "Strain lengths: %s\n"%([len(x) for x in st])
            for i, st in enumerate(st):
                s += "\t%d: %s\n"%(i, st.__repr__())
            s += u"\n"

        return s

    def save(self, fname):
        """
        Save the json representation of this cluster to a file.
        """
        # first get the json representation
        cluster_json = self.to_json()
        # write the representation to a temporary file, s.t. in case of an error
        # the old file is still there
        with open(fname+".tmp", "w") as f:
            f.write(cluster_json)
        # when no error occured move the temporary file to the new file
        os.rename(fname+".tmp", fname)
        logger.debug("Cluster saved  to: %s", fname)
    
    @classmethod
    def load(cls, fname):
        with open(fname, "r") as f:
            c = cls.from_json(f.read())
    
        return c

class MultiProcessCluster(Cluster):
    nrof_processes = 100
    # pool_cls = multiprocessing.Pool
    pool_cls = ThreadPool

    def compute_strains(self, handle, strains_to_compute):
        results = []
        strains = []
        with self.pool_cls(processes=self.nrof_processes) as pool:
            for i in range(strains_to_compute):
                try:
                    strains.append(Strain(self.data_source, handle, 
                            verified_users=self.verified_users))
                    results.append(pool.apply_async(strains[i].generate))
                except twitter.error.TwitterError as e:
                    logger.warning("Twitter error: %s", e)
                    if e.message == "Not authorized.":
                        continue
            
            # Wait for all the computations to complete
            for i, result in enumerate(results):
                s = result.get()
                strains[i].strain = s
                result.successful()
                # save the results
            if handle not in self.strains:
                self.strains[handle] = strains
            else:
                self.strains[handle] += strains

