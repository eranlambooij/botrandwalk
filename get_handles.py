from __future__ import print_function # make print statements in python2 ok
from datasource import DataSource, FavoritesDataSource
from graph_traversal import Cluster
from twitter_config import api_keys
import sys

if len(sys.argv) not in [2, 3]:
    print("Usage: %s <search_query> [<nrof_handles>=100]"%sys.argv[0])
    exit()
# 
count = 100
if len(sys.argv) == 3:
    count = int(sys.argv[2])

fsd = FavoritesDataSource(keys = api_keys)

new_tweets = fsd.get_search(sys.argv[1], count=count)

handles = []
for tweet in new_tweets:
    handles.append(tweet.user.screen_name)

for handle in handles:
    print(handle)
